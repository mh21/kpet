# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests expecting a match"""
from tests.test_integration import BEAKER_XML_J2
from tests.test_integration import IntegrationTests
from tests.test_integration import assets_mkdir
from tests.test_integration import kpet_run_generate


class IntegrationMatchSetsTests(IntegrationTests):
    """Integration tests expecting a match in cases"""

    def test_match_abstract_case(self):
        """Check abstract case's subcases are included"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                sets:
                    foo: "Lorem"
                    bar: "ipsum"
                    baz: "dolor"
                case:
                  universal_id: test_id
                  host_types: ^normal
                  location: somewhere
                  max_duration_seconds: 600
                  cases:
                    A:
                      name: A
                      universal_id: test_uid1
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      sets: foo
                      cases:
                        a:
                          name: a
                    B:
                      name: B
                      universal_id: test_uid2
                      maintainers:
                        - name: maint2
                          email: maint2@maintainers.org
                      sets: bar
                      cases:
                        b:
                          name: b
                """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Matches an abstract case
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-s", "foo",
                stdout_matching=r'.*<job>\s*HOST\s*A - a\s*</job>.*')
            # Matches another abstract case
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-s", "bar",
                stdout_matching=r'.*<job>\s*HOST\s*B - b\s*</job>.*')
            # Doesn't match any cases
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-s", "baz",
                stdout_matching=r'.*<job>\s</job>.*')

    def test_match_test_case(self):
        """Check a test case is included when matching"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                sets:
                    foo: "Lorem"
                    bar: "ipsum"
                    baz: "dolor"
                case:
                  universal_id: test_uid
                  host_types: ^normal
                  location: somewhere
                  max_duration_seconds: 600
                  cases:
                    A:
                      name: A
                      universal_id: test_uid1
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      sets: foo
                    B:
                      name: B
                      universal_id: test_uid2
                      maintainers:
                        - name: maint2
                          email: maint2@maintainers.org
                      sets: bar
                """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            # Matches a test case
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-s", "foo",
                stdout_matching=r'.*<job>\s*HOST\s*A\s*</job>.*')
            # Matches a different test case
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-s", "bar",
                stdout_matching=r'.*<job>\s*HOST\s*B\s*</job>.*')

    def test_match_not_subset_error(self):
        """
        Check we detect a test case with sets its super-case doesn't have
        """
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                sets:
                    foo: "Lorem"
                    bar: "ipsum"
                    baz: "dolor"
                case:
                  universal_id: test_uid
                  host_types: ^normal
                  cases:
                    A:
                      name: A
                      location: somewhere
                      universal_id: test_uid
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      sets:
                        - foo
                        - bar
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                          sets:
                            - foo
                            - baz
                """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-s", "foo", status=1,
                stderr_matching=r".* matches none of the "
                r"available sets: .*")

    def test_match_nonexistent_set_test_case_error(self):
        """
        Check we get an error when specifying an unknown set in a test case
        """
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                sets:
                    foo: "Lorem"
                    bar: "ipsum"
                    baz: "dolor"
                case:
                  universal_id: test_uid
                  host_types: ^normal
                  cases:
                    A:
                      name: A
                      location: somewhere
                      universal_id: test_uid
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      sets:
                        - foo
                        - bar
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                          sets:
                            - foo
                            - baz
                            - unknown
                """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-s", "foo", status=1,
                stderr_matching=r".* matches none of the "
                r"available sets: .*")

    def test_match_nonexistent_set_abstract_case_error(self):
        """
        Check we get an error when specifying an unknown set in abstract case
        """
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                sets:
                    foo: "Lorem"
                    bar: "ipsum"
                    baz: "dolor"
                case:
                  universal_id: test_uid
                  host_types: ^normal
                  cases:
                    A:
                      name: A
                      location: somewhere
                      universal_id: test_uid
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      sets:
                        - foo
                        - bar
                        - unknown
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                          sets:
                            - foo
                            - baz
                """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "-s", "foo", status=1,
                stderr_matching=r".* matches none of the "
                r"available sets: .*")
